#!/bin/bash

set -eu

git config user.email "carol.nichols@gmail.com"
git config user.name "CarolBot"

while IFS=',' read -r name url selector extra_cmd ; do
    echo "Checking $name"
    curl -s -A 'Block me if you need to. Just trying to help people. carol.nichols@gmail.com, https://gitlab.com/vaccinate-pa/vax-watcher' \
        $url \
        | ./bin/pup -i 2 $selector \
        | $extra_cmd \
        > "county-health-dept-pages/${name}.html"

    additions=`git diff --numstat | tail -n 1 | cut -f 1`

    if [[ additions && additions -gt 0 ]]; then
        git commit -am "Changes in $name to $url" || true
    elif [[ additions ]]; then
        git commit -am "Only deletes in $name to $url" || true
    fi

done < county-health-dept-pages.csv

url_host=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`
git remote set-url origin "https://gitlab-ci-token:${GITLAB_TOKEN}@${url_host}"
git push origin HEAD:main || true
