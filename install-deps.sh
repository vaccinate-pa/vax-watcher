#!/bin/bash

set -eu

mkdir -p bin

if [ ! -f "bin/pup" ]; then
  curl -L -o bin/pup.zip https://github.com/ericchiang/pup/releases/download/v0.4.0/pup_v0.4.0_linux_amd64.zip
  unzip -d bin bin/pup.zip
else
  echo "pup exists!"
fi

if [ ! -f "bin/oysttyer" ]; then
    curl -L -o bin/oysttyer.tar.gz https://github.com/oysttyer/oysttyer/archive/2.10.0.tar.gz
    tar -xzf bin/oysttyer.tar.gz
    mv oysttyer-2.10.0/oysttyer.pl bin/oysttyer
else
  echo "oysttyer exists!"
fi
