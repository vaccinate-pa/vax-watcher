#!/bin/bash

set -eu

while read -r rev; do
    msg=`git show --format=format:"%s" -s "$rev"`
    tweet="$msg difference: https://gitlab.com/vaccinate-pa/vax-watcher/-/commit/${rev}"
    ./bin/oysttyer -rc="$OYSTTYER_RC" -keyf="$OYSTTYER_KEYF" -status="$tweet"
done < <(git rev-list --reverse --grep Changes $CI_COMMIT_BEFORE_SHA..HEAD)
